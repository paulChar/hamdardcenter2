json.array!(@caregivers) do |caregiver|
  json.extract! caregiver, :id, :name, :phone, :birthday
  json.url caregiver_url(caregiver, format: :json)
end
