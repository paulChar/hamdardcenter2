json.extract! @client, :id, :name, :address, :phone, :hours, :emergency_contact, :emergency_contact_phone, :active, :birthdate, :services_started, :services_ended, :created_at, :updated_at
