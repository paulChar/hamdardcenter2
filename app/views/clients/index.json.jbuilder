json.array!(@clients) do |client|
  json.extract! client, :id, :name, :address, :phone, :hours, :emergency_contact, :emergency_contact_phone, :active, :birthdate, :services_started, :services_ended
  json.url client_url(client, format: :json)
end
