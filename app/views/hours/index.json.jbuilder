json.array!(@hours) do |hour|
  json.extract! hour, :id, :client_id, :caregiver_id, :time
  json.url hour_url(hour, format: :json)
end
