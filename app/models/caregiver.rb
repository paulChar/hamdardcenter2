class Caregiver < ActiveRecord::Base
	has_many :hours
	has_many :clients, through: :hours

	validates :name, presence: true
	validates :phone, presence: true
	
	def client_list
		list = Array.new
		self.clients.all.each do |c|
			if !list.include?( [c.name, c.id] )
				list << [c.name, c.id]
			end
		end
		return list
	end
	
end
