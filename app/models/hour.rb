class Hour < ActiveRecord::Base
	belongs_to :client
	belongs_to :caregiver

	validates :client_id, 
		presence: true, 
		numericality: { only_integer: true }
	validates :caregiver_id, 
		presence: true, 
		numericality: { only_integer: true }
	validates :time, 
		presence: true, 
		numericality: true

end
