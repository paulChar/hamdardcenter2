class Client < ActiveRecord::Base
	has_many :hours
	has_many :caregivers, through: :hours

	validates :name, presence: true
	validates :phone, presence: true

	def caregiver_list
		list = Array.new
		self.caregivers.all.each do |c|
			if !list.include?( [c.name, c.id] )
				list << [c.name, c.id]
			end
		end
		return list
	end

end