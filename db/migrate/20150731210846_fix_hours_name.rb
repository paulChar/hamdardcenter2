class FixHoursName < ActiveRecord::Migration
  def change
  	rename_column :hours, :hours, :time
  end
end
