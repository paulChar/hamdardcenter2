class CreateHours < ActiveRecord::Migration
  def change
    create_table :hours do |t|
      t.integer :client_id
      t.integer :caregiver_id
      t.float :hours

      t.timestamps null: false
    end
  end
end
