class AddClientAgreementToClient < ActiveRecord::Migration
  def change
    add_column :clients, :client_agreement, :date
  end
end
