class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :name
      t.string :address
      t.string :phone
      t.float :hours
      t.string :emergency_contact
      t.string :emergency_contact_phone
      t.boolean :active
      t.date :birthdate
      t.date :services_started
      t.date :services_ended

      t.timestamps null: false
    end
  end
end
