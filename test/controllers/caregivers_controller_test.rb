require 'test_helper'

class CaregiversControllerTest < ActionController::TestCase
  setup do
    @caregiver = caregivers(:one)
    @user = users(:one)
  end

  test "should get index as User" do # =========================================
  	sign_in @user 
    get :index
    assert_response :success
    assert_not_nil assigns(:caregivers)
  end
  test "should NOT get index as Guest" do
    get :index
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should get new as User" do # =========================================
    sign_in @user 
    get :new
    assert_response :success
  end
  test "should NOT get new as Guest" do
    get :new
    assert_response :redirect 
    assert_redirected_to new_user_session_path
  end

  test "should create caregiver as User" do # =================================
    sign_in @user
    assert_difference('Caregiver.count') do
      post :create, caregiver: { birthday: @caregiver.birthday, name: @caregiver.name, phone: @caregiver.phone }
    end

    assert_redirected_to caregiver_path(assigns(:caregiver))
  end
  test "should NOT create caregiver as Guest" do
    assert_no_difference('Caregiver.count') do
      post :create, caregiver: { birthday: @caregiver.birthday, name: @caregiver.name, phone: @caregiver.phone }
    end

    assert_redirected_to new_user_session_path
  end

  test "should show caregiver as User" do # ==============================
    sign_in @user
    get :show, id: @caregiver
    assert_response :success
    assert_equal 1, assigns(:client_list).count
  end
  test "should NOT show caregiver as Guest" do
    get :show, id: @caregiver
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end


  test "should get edit as User" do # =======================================
    sign_in @user
    get :edit, id: @caregiver
    assert_response :success
  end
  test "should NOT get edit as Guest" do
    get :edit, id: @caregiver
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should update caregiver as User" do # ===================================
    sign_in @user
    patch :update, id: @caregiver, caregiver: { birthday: @caregiver.birthday, name: @caregiver.name, phone: @caregiver.phone }
    assert_redirected_to caregiver_path(assigns(:caregiver))
  end
  test "should NOT update caregiver as Guest" do
    patch :update, id: @caregiver, caregiver: { birthday: @caregiver.birthday, name: @caregiver.name, phone: @caregiver.phone }
    assert_redirected_to new_user_session_path
  end

  test "should destroy caregiver as User" do # ====================================
    sign_in @user
    assert_difference('Caregiver.count', -1) do
      delete :destroy, id: @caregiver
    end

    assert_redirected_to caregivers_path
  end
  test "should NOT destroy caregiver as Guest" do
    assert_no_difference('Caregiver.count') do
      delete :destroy, id: @caregiver
    end

    assert_redirected_to new_user_session_path
  end
  
end
