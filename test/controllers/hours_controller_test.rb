require 'test_helper'

class HoursControllerTest < ActionController::TestCase
  setup do
    @hour = hours(:one)
    @admin = users(:one)
  end

  test "should get index as Admin" do # ==============================
    sign_in @admin
    get :index
    assert_response :success
    assert_not_nil assigns(:hours)
  end
  test "should NOT get index as Guest" do
    get :index
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should get new as Admin" do # ======================================
    sign_in @admin
    get :new
    assert_response :success
  end
  test "should NOT get new as Guest" do
    get :new
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should create hour as Admin" do #====================================
  	sign_in @admin
    assert_difference('Hour.count') do
      post :create, hour: { caregiver_id: @hour.caregiver_id, client_id: @hour.client_id, time: @hour.time }
    end

    assert_redirected_to hour_path(assigns(:hour))
  end
  test "should NOT create hour as Guest" do
    assert_no_difference('Hour.count') do
      post :create, hour: { caregiver_id: @hour.caregiver_id, client_id: @hour.client_id, time: @hour.time }
    end

    assert_redirected_to new_user_session_path
  end

  test "should show hour as Admin" do #===========================
    sign_in @admin
    get :show, id: @hour
    assert_response :success
  end
  test "should NOT show hour as Guest" do
    get :show, id: @hour
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should get edit as Admin" do #=============================
  	sign_in @admin
    get :edit, id: @hour
    assert_response :success
  end
  test "should NOT get edit as Guest" do 
    get :edit, id: @hour
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should update hour as Admin" do #==========================
  	sign_in @admin
    patch :update, id: @hour, hour: { caregiver_id: @hour.caregiver_id, client_id: @hour.client_id, time: @hour.time }
    assert_redirected_to hour_path(assigns(:hour))
  end
  test "should NOT update hour as Guest" do
    patch :update, id: @hour, hour: { caregiver_id: @hour.caregiver_id, client_id: @hour.client_id, time: @hour.time }
    assert_redirected_to new_user_session_path
  end

  test "should destroy hour as Admin" do#===========================
  	sign_in @admin
    assert_difference('Hour.count', -1) do
      delete :destroy, id: @hour
    end

    assert_redirected_to hours_path
  end
  test "should NOT destroy hour as Guest" do
    assert_no_difference('Hour.count') do
      delete :destroy, id: @hour
    end

    assert_redirected_to new_user_session_path
  end
  
end
