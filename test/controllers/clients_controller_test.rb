require 'test_helper'

class ClientsControllerTest < ActionController::TestCase
  setup do
  	@admin = users(:one)
    @client = clients(:one)
  end

  test "should get index as Admin" do#==========================
  	sign_in @admin 
    get :index
    assert_response :success
    assert_not_nil assigns(:clients)
    assert_equal 1, assigns(:upcoming_birthdays_clients).count #Tests upcoming Birthdays function
    assert_equal 1, assigns(:upcoming_birthdays_caregivers).count #Tests upcoming Birthdays function
    assert_equal 2, assigns(:upcoming_client_agreements).count #Tests upcoming client agreements function
  end
  test "should NOT get index as Guest" do
    get :index
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end


  test "should get new as Admin" do#================================
  	sign_in @admin 
    get :new
    assert_response :success
  end
  test "should NOT get new as Guest" do
    get :new
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should create client as Admin" do #=========================
  	sign_in @admin
    assert_difference('Client.count') do
      post :create, client: { active: @client.active, address: @client.address, birthdate: @client.birthdate, emergency_contact: @client.emergency_contact, emergency_contact_phone: @client.emergency_contact_phone, name: @client.name, phone: @client.phone, services_ended: @client.services_ended, services_started: @client.services_started }
    end

    assert_redirected_to client_path(assigns(:client))
  end
  test "should NOT create client as Guest" do
    assert_no_difference('Client.count') do
      post :create, client: { active: @client.active, address: @client.address, birthdate: @client.birthdate, emergency_contact: @client.emergency_contact, emergency_contact_phone: @client.emergency_contact_phone, name: @client.name, phone: @client.phone, services_ended: @client.services_ended, services_started: @client.services_started }
    end

    assert_redirected_to new_user_session_path
  end

  test "should show client as Admin" do #=============================
  	sign_in @admin
    get :show, id: @client
    assert_response :success
    assert_equal 1, assigns(:caregiver_list).count
    assert_equal 2, assigns(:caregiver_time_table).count
  end
  test "should NOT show client as Guest" do
    get :show, id: @client
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should get edit as Admin" do#=================================
  	sign_in @admin
    get :edit, id: @client
    assert_response :success
  end
  test "should NOT get edit as Guest" do
    get :edit, id: @client
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should update client as Admin" do#=========================
  	sign_in @admin
    patch :update, id: @client, client: { active: @client.active, address: @client.address, birthdate: @client.birthdate, emergency_contact: @client.emergency_contact, emergency_contact_phone: @client.emergency_contact_phone, name: @client.name, phone: @client.phone, services_ended: @client.services_ended, services_started: @client.services_started }
    assert_redirected_to client_path(assigns(:client))
  end
  test "should NOT update client as Guest" do
    patch :update, id: @client, client: { active: @client.active, address: "test", birthdate: @client.birthdate, emergency_contact: @client.emergency_contact, emergency_contact_phone: @client.emergency_contact_phone, name: @client.name, phone: @client.phone, services_ended: @client.services_ended, services_started: @client.services_started }
    @client.reload
    assert_not_equal @client.address, "test"
    assert_redirected_to new_user_session_path
  end

  test "should destroy client as Admin" do#============================
  	sign_in @admin
    assert_difference('Client.count', -1) do
      delete :destroy, id: @client
    end

    assert_redirected_to clients_path
  end
  test "should NOT destroy client as Guest" do
    assert_no_difference('Client.count') do
      delete :destroy, id: @client
    end

    assert_redirected_to new_user_session_path
  end
end
