require 'test_helper'

class ClientTest < ActiveSupport::TestCase
  def setup #=========================================
		@client = Client.new(
			name: "string",
			phone: "string",
			birthdate: 2015-05-20 )
	end #===============================================

	test "should be valid" do #-------------------------
		assert @client.valid?
	end
  test 'Name should be present' do 
  	@client.name = ''
		assert_not @client.valid?
  end
  test 'Phone should be pressent' do 
  	@client.phone = ''
		assert_not @client.valid?
  end #---------------------------------------------
end
