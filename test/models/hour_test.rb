require 'test_helper'

class HourTest < ActiveSupport::TestCase
	
	def setup #=========================================
		@hour = Hour.new(
			client_id: 1,
			caregiver_id: 2,
			time: 60126 )
	end #===============================================

	test "should be valid" do #-------------------------
		assert @hour.valid?
	end
  test 'Client id should be present' do 
  	@hour.client_id = ''
		assert_not @hour.valid?
  end
  test 'Caregiver id should be pressent' do 
  	@hour.caregiver_id = ''
		assert_not @hour.valid?
  end
  test 'Time should be present' do 
  	@hour.time = ''
		assert_not @hour.valid?
  end
  test 'Client id should be a integer' do 
  	@hour.client_id = 'f1'
		assert_not @hour.valid?
		@hour.client_id = '#'
		assert_not @hour.valid?
		@hour.client_id = '.'
		assert_not @hour.valid?
		@hour.client_id = '1'
		assert @hour.valid?
		@hour.client_id = '1.0'
		assert_not @hour.valid?
		@hour.client_id = '1.5'
		assert_not @hour.valid?  end
  test 'Caregiver id should be a integer' do 
  	@hour.caregiver_id = 'f1'
		assert_not @hour.valid?
		@hour.caregiver_id = '#'
		assert_not @hour.valid?
		@hour.caregiver_id = '.'
		assert_not @hour.valid?
		@hour.caregiver_id = '1'
		assert @hour.valid?
		@hour.caregiver_id = '1.0'
		assert_not @hour.valid?
		@hour.caregiver_id = '1.5'
		assert_not @hour.valid?
  end
  test 'Time should be a number' do 
  	@hour.time = 'f1'
		assert_not @hour.valid?
		@hour.time = '#'
		assert_not @hour.valid?
		@hour.time = '.'
		assert_not @hour.valid?
		@hour.time = '1'
		assert @hour.valid?
		@hour.time = '1.0'
		assert @hour.valid?
		@hour.time = '1.5'
		assert @hour.valid?
  end #---------------------------------------------

end
