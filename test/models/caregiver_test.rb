require 'test_helper'

class CaregiverTest < ActiveSupport::TestCase
  
  def setup #=========================================
		@caregiver = Caregiver.new(
			name: "string",
			phone: "string",
			birthday: 2015-05-20 )
	end #===============================================

	test "should be valid" do #-------------------------
		assert @caregiver.valid?
	end
  test 'Name should be present' do 
  	@caregiver.name = ''
		assert_not @caregiver.valid?
  end
  test 'Phone should be pressent' do 
  	@caregiver.phone = ''
		assert_not @caregiver.valid?
  end #---------------------------------------------


end
