# creates a array of birthdays and names list[0] => date list[1] => name
def upcoming_birthdays_clients
	current_date = Date.today
	list = Array.new
	Client.all.each do |b|
		birthday = Date.new(current_date.year, b.birthdate.month, b.birthdate.day) 
		if birthday >= current_date && birthday <= current_date + 30.days
			list << [ birthday, b.name, b.id ]
		end
	end
	return list.sort
end

def upcoming_birthdays_caregivers
	current_date = Date.today
	list = Array.new
	Caregiver.all.each do |b|
		birthday = Date.new(current_date.year, b.birthday.month, b.birthday.day) 
		if birthday >= current_date && birthday <= current_date + 30.days
			list << [ birthday, b.name, b.id ]
		end
	end
	return list.sort
end

def upcomming_client_agreement_renewals
	current_date = Date.today
	list = Array.new
	Client.all.each do |b|
		if !b.client_agreement.nil?
			if b.client_agreement <= current_date + 10.days
				list << [ b.name, b.id ]
			end
		end
	end
	return list
end